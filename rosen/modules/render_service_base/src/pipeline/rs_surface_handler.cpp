/*
 * Copyright (c) 2022-2023 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "pipeline/rs_surface_handler.h"

namespace OHOS {
namespace Rosen {
#ifndef ROSEN_CROSS_PLATFORM
void RSSurfaceHandler::SetConsumer(const sptr<IConsumerSurface>& consumer)
{
    consumer_ = consumer;
}
#endif

void RSSurfaceHandler::IncreaseAvailableBuffer()
{
    bufferAvailableCount_++;
}

void RSSurfaceHandler::ReduceAvailableBuffer()
{
    --bufferAvailableCount_;
}

void RSSurfaceHandler::SetGlobalZOrder(float globalZOrder)
{
    globalZOrder_ = globalZOrder;
}

float RSSurfaceHandler::GetGlobalZOrder() const
{
    return globalZOrder_;
}

#ifndef ROSEN_CROSS_PLATFORM
void RSSurfaceHandler::ReleaseBuffer(SurfaceBufferEntry& buffer)
{
    auto& consumer = GetConsumer();
    if (consumer != nullptr && buffer.buffer != nullptr) {
        auto ret = consumer->ReleaseBuffer(buffer.buffer, SyncFence::INVALID_FENCE);
        if (ret != OHOS::SURFACE_ERROR_OK) {
            RS_LOGD("RsDebug surfaceHandler(id: %{public}" PRIu64 ") ReleaseBuffer failed(ret: %{public}d)!",
                GetNodeId(), ret);
        }
        buffer.Reset();
    }
}

void RSSurfaceHandler::ConsumeAndUpdateBuffer(SurfaceBufferEntry buffer)
{
    if (!buffer.buffer) {
        return;
    }
    SetBufferSizeChanged(buffer.buffer);
    SetBuffer(buffer.buffer, buffer.acquireFence, buffer.damageRect, buffer.timestamp);
    SetCurrentFrameBufferConsumed();
    RS_LOGD("RsDebug surfaceHandler(id: %{public}" PRIu64 ") buffer update, "\
        "buffer timestamp = %{public}" PRIu64 " .", GetNodeId(), static_cast<uint64_t>(buffer.timestamp));
}

void RSSurfaceHandler::CacheBuffer(SurfaceBufferEntry buffer)
{
    bufferCache_[static_cast<uint64_t>(buffer.timestamp)] = buffer;
}

bool RSSurfaceHandler::HasBufferCache() const
{
    return bufferCache_.size() != 0;
}

RSSurfaceHandler::SurfaceBufferEntry RSSurfaceHandler::GetBufferFromCache(uint64_t vsyncTimestamp)
{
    RSSurfaceHandler::SurfaceBufferEntry buffer;
    for (auto iter = bufferCache_.begin(); iter != bufferCache_.end();) {
        if (iter->first < vsyncTimestamp) {
            ReleaseBuffer(buffer);
            buffer = iter->second;
            iter = bufferCache_.erase(iter);
        } else {
            break;
        }
    }
    return buffer;
}
#endif
}
}
