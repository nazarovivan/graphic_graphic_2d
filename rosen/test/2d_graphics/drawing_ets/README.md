Store test case code for the Drawing ETS interface
Usage:
    1) Copy the ets folders to the entry/src/main directory of the app  project. App project can refer to:
       https://gitee.com/openharmony/applications_app_samples/tree/master/code/BasicFeature/Native/NdkTsDrawing.
    2) Copy the rawfile file from the app sample project to the entry/src/main/resources/ directory. Rawfile path:
       https://gitee.com/openharmony/applications_app_samples/tree/master/code/BasicFeature/Native/NdkRenderNodeDrawing/entry/src/main/resources/rawfile
    3) Compile and build app project using DevEco Studio.